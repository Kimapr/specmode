iips={}
for k,v in ipairs({"interact","fly","fast","noclip","teleport"}) do
	minetest.register_privilege("spec_"..v,{
		description="Grant "..v.." after end of spectator mode",
		give_to_singleplayer=false,
		give_to_admin=false,
	})
	iips[v]="spec_"..v
end
minetest.register_privilege("spec",{
	description="Spectator mode indicator",
	give_to_singleplayer=false,
	give_to_admin=false
})
minetest.register_chatcommand("spectator",{
	description = "Toggle spectator mode. WARNING: DUMPS INVENTORY",
	func=function(name)
		local ref=minetest.get_player_by_name(name)
		if not ref then return end
		local privs=minetest.get_player_privs(name)
		if privs.spec then
			for k,v in pairs(iips) do
				privs[k]=privs[v]
				privs[v]=nil
			end
			privs.spec=nil
			ref:set_pos(minetest.string_to_pos(ref:get_meta():get_string("spec_pos")))
		else
			for k,v in pairs(iips) do
				privs[v]=privs[k]
				privs[k]=nil
			end
			privs.spec=true
			privs.interact=nil
			privs.fly=true
			privs.fast=true
			privs.noclip=true
			privs.teleport=true
			ref:get_meta():set_string("spec_pos",minetest.pos_to_string(ref:get_pos()))
		end
		minetest.set_player_privs(name,privs)
	end
})
